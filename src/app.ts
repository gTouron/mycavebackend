import express from "express";
import mysql from 'mysql';

const connection = mysql.createConnection({
    host: 'localhost',
    user: 'soyuz',
    password: 'soyuz',
    database: 'mycave'
});

connection.connect((err) => {
    if (err) {
        console.log('Error connecting to Db');
        return;
    }
    console.log('Connected !');   
});

connection.query('SELECT * FROM Bottles', (err, rows) => {
    if(err) throw err;

    console.log('Data received from Db:');
    console.log(rows);  
    
})

const app = express();

app.get('/', (req, res) => {
    res.send('Hello');
});

app.listen(5000, () => console.log('Server running'));